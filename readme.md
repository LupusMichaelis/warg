# Warg

A tool to check out regular expressions

Only working on Linux using X11R6 server.

## Run and hack

The `run` tool is using [Docker](https://www.docker.com/) for ease of isolation from host
system.

Just build and run the app:

```bash
./run it
```

To listen changes on disk to compile automatically:

```bash
./run workshop
```

## Manual building

See [Dockerfile](./.docker/Dockerfile)

## Systemwide installation

There is no installation process yet, just use the exe as it is or the container
